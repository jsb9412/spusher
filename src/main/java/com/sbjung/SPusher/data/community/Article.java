package com.sbjung.SPusher.data.community;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Builder
@Document
@Data
public class Article {

    @Id
    private ObjectId id;

    private Long articleNo; // PK

    private String title;

    private String subTitle;

    private String content;

    private String author;  // TODO : authorNo 또는 별도 class로 관리

    private Long boardNo;   // 게시판

    // Count는 어떻게 처리할 지 고민해보자

    @CreatedDate
    private Date regDt;

    @LastModifiedDate
    private Date updDt; // 시스템에 의해 필드가 수정된 경우

    private Date modDt; // 사용자가 직접 수정한 경우

}
