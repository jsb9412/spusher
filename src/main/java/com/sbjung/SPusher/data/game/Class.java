package com.sbjung.SPusher.data.game;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Class {

    private String name;
    private ClassFamily family;

    public enum ClassFamily {

        WARRIER("Warrier"),
        MAGICIAN("Magician"),
        ARCHER("Archer"),
        THIEF("Thief"),
        PIRATE("Pirate");

        final private String name;

        ClassFamily(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

    }

}
