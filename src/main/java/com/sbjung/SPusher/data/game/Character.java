package com.sbjung.SPusher.data.game;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

@Data
public class Character {

    private int level;

    private String name;

    private Class classType;


    @CreatedDate
    private Date regDt;

    @LastModifiedDate
    private Date updDt;

    private Date growDt;    // 마지막으로 레벨업 한 시간

}
