package com.sbjung.SPusher.schedule;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sbjung.SPusher.data.community.Article;
import com.sbjung.SPusher.repo.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ArticlePusher {

    private Long articleNo = 0L;

    @Resource
    private ArticleRepository articleRepository;

    @Scheduled(fixedRate = 6000) // 마지막 배치 Task의 시작 시간 부터 1분
    public void push() {
        Date currentDt = new Date();
        log.info("Push job started at : " + currentDt.toString());

        this.save();
    }

    private void save() {
        Article article = Article.builder()
                .articleNo(articleNo++)
                .author("JungSooBin")
                .title("게시글 제목 : " + articleNo)
                .content("게시글 내용인데 뭘 적어야할지 모르겠다")
                .build();

        articleRepository.save(article);
    }
}
