package com.sbjung.SPusher.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.sbjung.SPusher.data.community.Article;

@Repository("ArticleRepository")
public interface ArticleRepository extends MongoRepository<Article, ObjectId>, ArticleRepositoryCustom {

    Article findByArticleNo(Long articleNo);


}
