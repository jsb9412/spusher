package com.sbjung.SPusher.repo;

import com.sbjung.SPusher.data.community.Article;

public interface ArticleRepositoryCustom {

    Article findListWithPaging();
}
