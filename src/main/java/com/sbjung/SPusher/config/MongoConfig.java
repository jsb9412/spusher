package com.sbjung.SPusher.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;

@PropertySource("classpath:application.properties")
@Configuration
public class MongoConfig extends AbstractMongoConfiguration {
//public class MongoConfig {





//    @Bean
//    public MongoClient mongo() {
//        return new MongoClient("localhost");
//    }
//
//    @Bean
//    public MongoTemplate mongoTemplate() throws Exception {
//        return new MongoTemplate(mongo(), "test");
//    }


    @Value("${spring.data.mongodb.port}")
    public int port;

//    @Value("${config.mongo.dbname}")
    public String database = "sbjung";

    @Bean
    @Override
    public MongoClient mongoClient() {

        MongoClientOptions.Builder options = new MongoClientOptions.Builder()
                .connectionsPerHost(10)
                .threadsAllowedToBlockForConnectionMultiplier(3);

        return new MongoClient("127.0.0.1", port); // TODO : config 파일로 관리할 것
    }

    @Bean
    @Override
    protected String getDatabaseName() {
        return database;
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory(), mappingMongoConverter());
    }

    @Bean
    public MongoDbFactory mongoDbFactory() {
        return new SimpleMongoDbFactory(mongoClient(), database);
    }

}
