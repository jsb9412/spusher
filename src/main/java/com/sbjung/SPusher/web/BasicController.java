package com.sbjung.SPusher.web;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class BasicController {

    @RequestMapping("/start")
    public String startUp() {
        return "This Application is Started";
    }

//    @RequestMapping("/error")
//    public String getError() {
//        return "넌 뭘 잘못한거냐 대체";
//    }
}
